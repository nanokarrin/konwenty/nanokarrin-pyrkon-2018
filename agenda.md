# Propozycja NanoKarrin dla Pyrkonu 2018

## Nasza agenda
Na Pyrkonie 2017 organizowaliśmy stoisko w Strefie Fantastycznych Inicjatyw i jedną atrakcję, także w SFI. W najbliższej edycji Pyrkonu mieliśmy plan powtórzyć tę operację, przy czym chcieliśmy umieścić stoisko w bardziej optymalnym miejscu (ostatnio mieliśmy wokół siebie mało miejsca) i zrobić w miarę możliwości więcej atrakcji, w tym koncert. Omawialiśmy te pomysły wstępnie z Piką po zakończeniu konwentu. Jesteśmy otwarci na możliwości rozszerzenia współpracy z Pyrkonem.

## Co możemy zrobić

### Koncert
* Repertuar koncertu, zgodnie z charakterem grupy, jest animowo-bajkowy z akcentami z gier i filmów, na koncercie występuje zwykle od 4 do 8 wokalistów śpiewających do gotowych podkładów. Robimy nieśmiałe próby z koncertami akustycznymi, ale ze względu na rozproszenie ludzi w grupie jest to dość trudne. 
* Mamy sporo swojego sprzętu, mikser i mikrofony, nie mamy natomiast żadnych sensownych przodów. Ostatnio w SFI nagłaśnialiśmy prelekcję małymi, uniwersalnymi głośnikami Behringera, ale na koncert mogą być za słabe - chociaż dużo zależy od tego co, gdzie i kiedy. 
* Koncert trwa od godziny do dwóch, potrzebny jest też czas na soundcheck i próbę.

### Inne atrakcje, warsztaty, panele
Członkowie grupy prowadzą często różne atrakcje związane z dubbingiem, takie jak warsztaty tekściarskie, wokalne, realizacji dźwięku, konkursy czy panele dyskusyjne. 

### Stoisko / salka tematyczna
Na naszym stoisku (które może też znajdować się w wydzielonej sali, jeśli są takie możliwości) uczestnicy konwentu mają okazję nagrać się do przygotowanych przez nas materiałów (aktorsko i wokalnie), usłyszeć swój głos, zobaczyć fragment realizacji dźwięku i zobaczyć efekty tej pracy. Poza tym mogą też po prostu porozmawiać z członkami grupy na temat jej działalności, jak i dubbingu w ogóle. Na ostatnim Pyrkonie ta atrakcja cieszyła się dużym powodzeniem, większym niż się spodziewaliśmy.

## Kontakt
W sprawach organiacyjnych najlepiej kontaktować się z liderką grupy, która dowodzi też bytnością NK na Pyrkonie: Jola "Pchełka" Dereń, j.deren@wp.eu, tel: 793954036 (po 25 września)